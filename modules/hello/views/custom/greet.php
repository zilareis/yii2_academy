<h1>Hello world from custom module!</h1>
<h2><?=Yii::t('hello', 'Translate')?> </h2>

<?= \lajax\languagepicker\widgets\LanguagePicker::widget([
    'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_BUTTON,
    'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_SMALL
]); ?>
