<?php
namespace app\modules\hello;
use Yii;

class Hello extends \yii\base\Module
{
    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations['hello'])) {
            Yii::$app->i18n->translations['hello'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en',
                'basePath' => '@app/modules/hello/messages'
            ];
        }
    }

}