<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%items}}`.
 */
class m190513_133330_create_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%items}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'description' => $this->text(),
            'photo' => $this->string(255),
            'category_id' => $this->integer(),
            'number' => $this->integer(),
        ]);

        $this->createTable('{{%categories}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);

        $this->createIndex(
            'idx-items-category_id',
            'items',
            'category_id'
        );

        $this->addForeignKey(
            'fk-items-category_id',
            'items',
            'category_id',
            'categories',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%items}}');
        $this->dropTable('{{%categories}}');

        $this->dropForeignKey(
            'fk-items-category_id',
            'items'
        );

        $this->dropIndex(
            'idx-items-category_id',
            'items'
        );
    }
}
